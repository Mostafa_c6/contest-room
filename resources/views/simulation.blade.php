<x-guest-layout>
    <div class="relative flex  min-h-screen bg-gray-100 dark:bg-gray-900 pt-32">
        @if (Route::has('login'))
        <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            @auth
            <a href="{{ url('/dashboard') }}" class="text-sm text-gray-700 underline">Dashboard</a>
            @else
            <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Login</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
            @endif
            @endauth
        </div>
        @endif

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="flex sm:pt-0">
                <img src="/logo-big.png" class="w-48 mx-auto" />
            </div>

            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg ">
                <div class="px-10 py-5" x-data="{code: 'test', count: 10}">
                    <h1 class="text-2xl font-medium my-5">User Simulation</h1>
                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="code" value="{{ __('Code') }}" />
                        <x-jet-input id="code" type="text" class="mt-1 block w-full" autocomplete="test-code"
                            x-model="code" />
                        <x-jet-input-error for="code" class="mt-2" />
                    </div>

                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="how" value="{{ __('Count') }}" />
                        <x-jet-input id="phone" type="number" class="mt-1 block w-full" autocomplete="new-phone"
                            x-model="count" />
                        <x-jet-input-error for="phone" class="mt-2" />
                    </div>
                    <div class="mt-5">
                        <x-jet-button @click="makeCalls(code, count)">
                            {{ __('Run!') }}
                        </x-jet-button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        let counter =  1;
        const seed = Math.floor(Math.random() * 1000);

        function makeCalls(code, count) {
            for (let i = 0; i < count; i++) {
                    postData('/api/entrypoint', 
                    {code: code,phone: '912' + '-' + seed + '-' + counter++}
                ).then(data => console.log(data));
            }
        }

        async function postData(url, data = {}) {
            const response = await fetch(url, {
                method: 'POST', 
                headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
                },
                body: JSON.stringify(data)
            });
            return response.json();
        }

    </script>
</x-guest-layout>