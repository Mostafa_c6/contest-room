<x-jet-form-section submit="createContest">
    <x-slot name="title">
        {{ __('Create Contest') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Create a new contest right in here.') }}
    </x-slot>

    <x-slot name="form">
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="code" value="{{ __('Code') }}" />
            <x-jet-input id="code" type="text" class="mt-1 block w-full" wire:model.defer="state.code"
                autocomplete="new-code" />
            <x-jet-input-error for="state.code" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="limit" value="{{ __('Limit') }}" />
            <x-jet-input id="limit" type="text" class="mt-1 block w-full" wire:model.defer="state.limit"
                autocomplete="new-limit" />
            <x-jet-input-error for="state.limit" class="mt-2" />
        </div>
    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="created">
            {{ __('Created.') }}
        </x-jet-action-message>

        <x-jet-button>
            {{ __('Create') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>