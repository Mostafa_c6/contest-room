<?php

namespace Tests\Feature;

use App\Models\Contest;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EntriesPageTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_admin_can_get_entries_page()
    {
        $contest = Contest::factory()->create();

        $this->actingAs(User::factory()->create())
            ->get("/contests/{$contest->code}/entries")
            ->assertSuccessful();
    }

    /** @test */
    public function guest_cannot_get_entries_page()
    {
        $contest = Contest::factory()->create();

        $this->get("/contests/{$contest->code}/entries")
            ->assertRedirect();
    }
}
