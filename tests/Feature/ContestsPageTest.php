<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ContestsPageTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_admin_can_get_contests_page()
    {
        $this->actingAs(User::factory()->create())
            ->get('/contests')
            ->assertSuccessful();
    }

    /** @test */
    public function guest_cannot_get_contests_page()
    {
        $this->get('/contests')
            ->assertRedirect('/login');
    }
}
