<?php

namespace Tests\Feature;

use App\Models\Contest;
use App\Models\Entry;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Fortify\Features;
use Tests\TestCase;

class EnterContestTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_should_be_accepeted_to_enter_an_existing_contest()
    {
        $contest = Contest::factory()->create();

        $input = ['code' => $contest->code, 'phone' => '9120000000'];

        $this->postJson("/api/entrypoint", $input)
            ->assertStatus(202);
    }


    /** @test */
    public function user_should_enter_a_contest_as_first_winner_and_win()
    {
        $contest = Contest::factory()
            ->create(['limit' => 5]);


        $input = ['code' => $contest->code, 'phone' => '9120000000'];

        $this->postJson("/api/entrypoint", $input)
            ->assertStatus(202);

        // waiting...

        $this->assertTrue($contest->winners()->wherePhone('9120000000')->exists());
    }

    /** @test */
    public function user_should_enter_a_contest_as_last_winner_and_win()
    {
        $contest = Contest::factory()
            ->has(Entry::factory()->winner()->count(4))
            ->create(['limit' => 5]);


        $input = ['code' => $contest->code, 'phone' => '9120000000'];

        $this->postJson("/api/entrypoint", $input)
            ->assertStatus(202);

        // waiting...

        $this->assertTrue($contest->winners()->wherePhone('9120000000')->exists());
        $this->assertEquals($contest->fresh()->winnersCount(), 5);
    }

    /** @test */
    public function user_should_enter_a_contest_late_and_lose()
    {
        $contest = Contest::factory()
            ->has(Entry::factory()->winner()->count(5))
            ->create(['limit' => 5]);


        $input = ['code' => $contest->code, 'phone' => '9120000000'];

        $this->postJson("/api/entrypoint", $input)
            ->assertStatus(202);

        // waiting...

        $this->assertFalse($contest->winners()->wherePhone('9120000000')->exists());

        $this->assertEquals($contest->fresh()->winnersCount(), 5);
    }

    /** @test */
    public function invalid_input_is_rejected()
    {
        $input = ['phone' => '12334'];

        $this->postJson("/api/entrypoint", $input)
            ->assertStatus(422);
    }

    /** @test */
    public function non_existing_code_will_be_accepted_but_later_ignored()
    {
        $input = ['code' => 'what contest??', 'phone' => '9120000000'];

        $this->postJson("/api/entrypoint", $input)
            ->assertStatus(202);
    }

    /** @test */
    public function duplicate_entry_will_be_accepted_but_later_ignored()
    {
        $contest = Contest::factory()
            ->has(Entry::factory()->winner()->count(2))
            ->create(['limit' => 10]);


        $input = ['code' => $contest->code, 'phone' => '9120000000'];

        $this->postJson("/api/entrypoint", $input)
            ->assertStatus(202);

        // duplicate entry, accepted but ignored!
        $this->postJson("/api/entrypoint", $input)
            ->assertStatus(202);

        // waiting...

        $this->assertEquals(1, $contest->winners()->wherePhone('9120000000')->count());

        $this->assertEquals($contest->fresh()->winnersCount(), 3);
    }

}
