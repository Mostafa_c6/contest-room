<?php

namespace Tests\Feature;

use App\Models\Contest;
use App\Models\Entry;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CheckForWinnerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function should_get_win_status_for_win()
    {
        $contest = Contest::factory()->create();

        $contest->entries()->create(['phone' => '9120000', 'winner' => true]);

        $this->call('GET', '/api/entries/status', ['phone' => '9120000', 'code' => $contest->code])
            ->assertJson(['status' => 'win']);
    }

    /** @test */
    public function should_get_loss_status_for_loss()
    {
        $contest = Contest::factory()
            ->has(Entry::factory()->winner()->count(5))
            ->create(['limit' => 5]);

        $contest->entries()->create(['phone' => '9120000', 'winner' => false]);

        $this->call('GET', '/api/entries/status', ['phone' => '9120000', 'code' => $contest->code])
            ->assertJson(['status' => 'loss']);
    }

    /** @test */
    public function should_get_running_status_when_contest_is_not_over()
    {
        $contest = Contest::factory()
            ->has(Entry::factory()->winner()->count(3))
            ->create(['limit' => 5]);

        $this->call('GET', '/api/entries/status', ['phone' => '9120000', 'code' => $contest->code])
            ->assertJson(['status' => 'running']);
    }

    /** @test */
    public function should_get_404_when_contest_doesnt_exist()
    {
        $this->call('GET', '/api/entries/status', ['code' => 'no contest'])
            ->assertNotFound();
    }
}
