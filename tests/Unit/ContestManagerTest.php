<?php

namespace Tests\Unit;

use App\Http\Livewire\ContestManager;
use App\Models\Contest;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Tests\TestCase;

class ContestManagerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_contest_via_form()
    {
        Livewire::test(ContestManager::class)
            ->set('state', ['code' => 'test', 'limit' => 100])
            ->call('createContest');

        $this->assertTrue(Contest::whereCode('test')->whereLimit(100)->exists());
    }

    /** @test */
    public function can_delete_contest()
    {
        $contest = Contest::factory()->create();

        Livewire::test(ContestManager::class)
            ->call('deleteContest', $contest->id);

        $this->assertNull($contest->fresh());
    }
}
