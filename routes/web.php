<?php

use App\Http\Controllers\AdminEntryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('/contests', function () {
        return view('contest.index');
    })->name('contests.index');

    Route::get('/contests/{contest:code}/entries', [AdminEntryController::class, 'index'])
        ->name('entries.index');
});

if (app()->environment('local')) {
    Route::get('/simulation', function () {
        return view('simulation');
    });
}
