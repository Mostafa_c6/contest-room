<?php

namespace App\Http\Livewire;

use App\Models\Contest;
use Livewire\Component;

class ContestManager extends Component
{
    public $state = [
        'code' => '',
        'limit' => ''
    ];

    protected $rules = [
        'state.code' => 'required|unique:contests,code',
        'state.limit' => 'required|int|min:1',
    ];

    public function render()
    {
        return view(
            'contest.contest-manager',
            [
                'contests' => $this->contests()
            ]
        );
    }

    public function contests()
    {
        return Contest::latest()->get();
    }

    public function createContest()
    {
        $this->validate();

        Contest::create($this->state);

        $this->emit('created');

        $this->reset();
    }

    public function deleteContest($id)
    {
        Contest::find($id)->delete();
    }
}
