<?php

namespace App\Http\Controllers;

use App\Models\Contest;

class AdminEntryController extends Controller
{
    public function index(Contest $contest)
    {
        return view('entry.index', [
            'entries' => $contest->entries()->orderBy('id')->with('contest')->get()
        ]);
    }
}
