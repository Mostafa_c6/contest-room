<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContestEntrypointController extends Controller
{
    public function __invoke(Request $request)
    {
        Validator::make($request->input(), [
            'code' => 'required',
            'phone' => 'required'
        ])->validate();

        $input = $request->only(['code', 'phone']);

        ProcessEntry::dispatch($input);

        return response()->json(['status' => 'Accepted.'], 202);
    }
}
