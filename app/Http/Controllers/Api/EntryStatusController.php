<?php

namespace App\Http\Controllers\Api;

use App\ContestRoom\QueryEntryStatus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EntryStatusController extends Controller
{
    public function __invoke(Request $request)
    {
        $entry = (object)([
            'code' => $request->query('code'),
            'phone' => $request->query('phone'),
        ]);

        return ['status' => app(QueryEntryStatus::class)->query($entry)];
    }
}
