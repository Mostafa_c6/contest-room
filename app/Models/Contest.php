<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contest extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'limit'
    ];

    public function entries()
    {
        return $this->hasMany(Entry::class);
    }

    public function winners()
    {
        return $this->entries()->winner();
    }

    public function winnersCount()
    {
        return $this->winners_count;
    }
}
