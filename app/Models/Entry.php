<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use HasFactory;

    protected $fillable = [
        'winner',
        'phone'
    ];

    public function contest()
    {
        return $this->belongsTo(Contest::class);
    }

    public function scopeWinner($query)
    {
        return $query->whereWinner(true);
    }
}
