<?php

namespace App\ContestRoom;

final class EntryStatus
{
    const WIN = 'win';
    const LOSS = 'loss';
    const RUNNING = 'running';
}
