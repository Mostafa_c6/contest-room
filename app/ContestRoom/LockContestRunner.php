<?php

namespace App\ContestRoom;
use App\Models\Contest;
use Illuminate\Support\Facades\DB;

class LockContestRunner implements ContestRunner
{
    public function attempt(object $entry)
    {
        DB::beginTransaction();
        $contest = Contest::whereCode($entry->code)->lockForUpdate()->first();

        if (!$contest) {
            return;
        }
        // reject duplicate entry 
        if ($contest->entries()->wherePhone($entry->phone)->exists()) {
            return;
        }

        $isWinner = $contest->winnersCount() < $contest->limit;


        $contest->entries()->create([
            'phone' => $entry->phone,
            'winner' => $isWinner,
        ]);

        DB::commit();
    }
}
