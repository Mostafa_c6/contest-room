<?php

namespace App\ContestRoom;

interface ContestRunner
{
    public function attempt(object $entry);
}
