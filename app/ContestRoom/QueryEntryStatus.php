<?php

namespace App\ContestRoom;

use App\Models\Contest;

class QueryEntryStatus
{
    /**
     * Query entry status 
     *
     * @param object $entry
     * @throws Database\Eloquent\ModelNotFoundException
     * @return string
     */
    public function query(object $entry)
    {
        $contest = Contest::whereCode($entry->code)->firstOrFail();

        $entry = $contest->entries()->wherePhone($entry->phone)->first();

        if (!$entry) {
            return EntryStatus::RUNNING;
        }

        return  $entry->winner ? EntryStatus::WIN : EntryStatus::LOSS;
    }
}
