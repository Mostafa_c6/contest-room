<?php

namespace App\ContestRoom;
use App\Models\Contest;

class SimpleContestRunner implements ContestRunner
{
    public function attempt(object $entry)
    {
        $contest = Contest::whereCode($entry->code)->first();

        if (!$contest) {
            return;
        }
        // reject duplicate entry 
        if ($contest->entries()->wherePhone($entry->phone)->exists()) {
            return;
        }

        $isWinner = $contest->winnersCount() < $contest->limit;


        $contest->entries()->create([
            'phone' => $entry->phone,
            'winner' => $isWinner,
        ]);
    }
}
