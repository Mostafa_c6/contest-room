<?php

namespace App\Jobs;

use App\ContestRoom\ContestRunner;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessEntry implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private object $entry;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $entry)
    {
        $this->entry = (object) $entry;
    }



    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        app(ContestRunner::class)->attempt($this->entry);
    }
}
